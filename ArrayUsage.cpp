// ArrayUsage.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>
using namespace std;

int getDate();
void printSumm();

int main()
{
    const int arraySize = 5;
    int arrayTest[arraySize][arraySize];
	int summ = 0;

	for (int i = 0; i < arraySize; i++)
	{
		for (int j = 0; j < arraySize; j++)
		{
			arrayTest[i][j] = i + j;
			cout << arrayTest[i][j];
		}
		cout << "\n";
	}
	for (int j = 0; j < arraySize; j++)
	{
		summ += arrayTest[getDate() % arraySize][j];
	}
	cout << summ;
}

int getDate() {
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	return buf.tm_mday;
}



